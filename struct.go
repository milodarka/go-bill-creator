package main

import (
	"fmt"
	"io/ioutil"
)

type bill struct {
	name string
	item map[string]float64
	tip  float64
}

//make new bills

func newBill(name string) bill {
	return bill{
		name: name,
		item: map[string]float64{},
		tip:  0,
	}
}

//format the bill(asociated with bill class)

func (b *bill) format() string {
	fs := "Bill brakedown; \n"
	var total float64 = 0
	for k, v := range b.item {
		fs += fmt.Sprintf("%-25v ... $%v\n", k+":", v)
		total += v
	}
	//add tip
	fs += fmt.Sprintf("%-25v ... $%0.2f\n", "tip:", b.tip)

	fs += fmt.Sprintf("%-25v ... $%0.2f", "TOTAL:", total+b.tip)
	return fs
}

func (b *bill) updateTip(tip float64) {
	b.tip = tip
}

func (b *bill) addItem(name string, price float64) {
	b.item[name] = price
}

func (b *bill) save() {
	data := []byte(b.format())

	err := ioutil.WriteFile("bills/"+b.name+".txt", data, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("bill was saved to file")
}
